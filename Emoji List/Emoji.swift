//
//  Emoji.swift
//  Emoji List
//
//  Created by umar on 10/13/17.
//  Copyright © 2017 iOS. All rights reserved.
//

import Foundation

struct Emoji {
    var symbol: String
    var name: String
    var description: String
    var usage: String
    
    init(symbol: String, name: String, description: String, usage: String) {
        self.symbol = symbol
        self.name = name
        self.description = description
        self.usage = usage
    }
}
